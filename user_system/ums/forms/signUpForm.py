from django import forms

class SignUpForm( forms.Form ):

    firstname = forms.CharField(label=("First Name"), max_length=50)
    lastname  = forms.CharField(label=("Last Name"), max_length=50)
    age       = forms.IntegerField(label="Age")
    email     = forms.CharField(label=("Email Id"), max_length=50)

    def __init__(self, request=None, *args, **kwargs):

        super( SignUpForm, self ).__init__(*args, **kwargs)

    def clean(self):
        fname = self.cleaned_data.get('firstname')
        lname = self.cleaned_data.get('lastname')
        age      = self.cleaned_data.get('age')
        email    = self.cleaned_data.get('email')

        print( fname )
        print( lname )
        print( age )
        print( email )






