from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .forms.signUpForm import SignUpForm
# Create your views here.
from django.template import Context, RequestContext, loader

from .models import UserDetails
import json

def login( request ):

     print("Inside login view-----")

     return render( request,
                    'homePage.html'
                  )

def signUp( request ):

    print("-----Inside Sign Up view-----")

    form = SignUpForm()

    t = loader.get_template('signUp.html')
    c = RequestContext(request, { 'form' : form } )
    response = HttpResponse(t.render(c))

    return response


def submitUserDetails( request ):

    print("------Inside submitUserDetails-----------")

    form = SignUpForm( data = request.GET )

    if form.is_valid():

        # save user into db using models here

        fname = form.cleaned_data['firstname']
        lname = form.cleaned_data['lastname']
        age = form.cleaned_data['age']
        email = form.cleaned_data['email']

        uDetails = UserDetails( firstname = fname,
                                lastname  = lname,
                                age       = age,
                                email     = email
                              )

        uDetails.save()

        return render( request,
                       'userSaved.html'
                     )



    return render( request,
                   ' <html> User Not Saved! </html> '
                 )


def adminPanel( request ):

    print("-------Inside admin Panel--------")

    return render( request,
                   'adminPanel.html'
                 )

def getAllUsers( request ):

    print("-----Inside getAllUsers--------")

    response = []
    rows = UserDetails.objects.all()

    for row in rows:

        response.append( { 'firstname' : row.firstname ,
                            'lastname' : row.lastname,
                            'age'      : row.age,
                            'email'    : row.email
                         }
                       )


    return HttpResponse( json.dumps( response ) )

