
console.info( "Scrip File Loaded" );

function getAllUsers()
{

    $.getJSON( 'getAllUsers', function(data)
           {
               console.info( "----------RESPONSE-----------")
               console.info( data );
               console.info( typeof(data) );

               fillGetAllUsersDiv(data);
           }
         );

}

function fillGetAllUsersDiv(data)
{

  var header = [];
  var rows = [];
  var i = 0;
  var j = 0;

  header[i++] = '<tr>';
  header[i++] = '<th> First Name </th>'
  header[i++] = '<th> Last Name  </th>'
  header[i++] = '<th> Age  </th>'
  header[i++] = '<th> Email  </th>'
  header[i++] = '</tr>';

  
  $('#usersidHeader').html( header.join('') );
  
  i = 0;


  for(var v = 0 ; v < data.length ; v++)
  {
    rows[i++] = '<tr>'
    rows[i++] = '<td>' + data[v].firstname + '</td>'
    rows[i++] = '<td>' + data[v].lastname + '</td>'
    rows[i++] = '<td>' + data[v].age + '</td>'
    rows[i++] = '<td>' + data[v].email + '</td>'
    rows[i++] = '</tr>'
  }
  
  $('#usersid').html(rows.join(''));

}
